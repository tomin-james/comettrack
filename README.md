# CometTrack

Deep learning powered pipeline to detect comets from NASA SOHO C2 data. 
ESA-NASA's SOHO mission was launched to study the outer layer of the Sun, called the corona
as well as to give real time observations to study space weather.
The Large Angle and Spectrometric Coronagraph (LASCO) on board the Solar and Heliospheric Observatory satellite (SOHO) consists of three solar coronagraphs with nested fields of view. The coronagraph artifically eclipse the sun, so that fainter parts of the corona is visible.
The LASCO C2, has detected many interplanetary comets over the many years of the operation.
 These are called sun grazing comets and its perihelion lies very close to the surface of the sun. Majority of them
belong to Kreutz family, which is thought to be fragments of a very big comet that broke up due to gravitational forces of the Sun.

![alt text](imgs/1.jpeg "Sungrazer comet")


## Description
Most sungrazer comets are faint and is visually very hard to recognize. 
The utility of a deep learning based approach is to use the labeled pixel informations regarding the comets
and try to develop a mapping function which will help the model recognize them.
For a comet to be detected it should be visible in atleast 3 consecutive images, taken 12 minutes apart, though there are cases 
where consecutive images are not available.

![alt text](imgs/2.png "Training sample")
