import numpy as np
import glob
import torch.nn as nn
import kornia as K
import torch
from torch.utils.data import Dataset, DataLoader
import pandas as pd
import numpy.typing as npt
from typing import List, Dict, Tuple
from comettrack.DataUtilities import circular_mask, circular_mask_vectorise, make_mask, create_comet_sequences, img_seq_load, vector_to_heatmaps
from comettrack.DataUtilities import RESIZE_FRAC, RAW_IMG_SIZE


def bck_sub_circular_normalize_batch(img: torch.tensor, flux_lim: float) -> npt.ArrayLike:
    '''
    The SOHO LASCO C2 images exhibit circular symmetrey.
    To exploit this fact, this function does background substraction
    on circular slices.

    The slice dimensions can be controlled below

    '''
    # convert the tensor to numpy array
    img_np = K.utils.tensor_to_image(img, keepdim=False)
    # since the already existing masks may have been eroded away
    # we re-apply the mask

    # nb = img_np.shape[0]

    nf = img_np.shape[-1]

    img_cube = np.empty((RAW_IMG_SIZE, RAW_IMG_SIZE, nf))

    # for jj in range(nb):
    # img_b = img_np[jj, :, :, :]
    for ii in range(nf):
        img_ = img_np[:, :, ii]
        ref_img = circular_mask(img_)

        inner_r = list(range(0, 650, 5))
        outer_r = list(range(10, 655, 5))
        start_mean = ref_img.mean()
        while True:
            for r, r_ in zip(inner_r, outer_r):
                mk = make_mask(r, r_, RAW_IMG_SIZE)
                img_[mk] -= img_[mk].mean()
                # img[mk] = img[mk].clip(min=0)
            current_mean = img_.mean()
            if (abs(current_mean) > flux_lim):
                start_mean = current_mean
            else:
                break
        img_cube[:, :, ii] = img_
        # img = normalize_image(img,1)
    return K.utils.image_to_tensor(img_cube, keepdim=True)


def bck_sub_circular_normalize_batch_vectorise(img: torch.tensor, flux_lim: float) -> npt.ArrayLike:
    '''
    The SOHO LASCO C2 images exhibit circular symmetrey.
    To exploit this fact, this function does background substraction
    on circular slices.

    The slice dimensions can be controlled below

    This is a vectorise version

    '''
    # convert the tensor to numpy array
    img_np = K.utils.tensor_to_image(img, keepdim=False)
    # since the already existing masks may have been eroded away
    # we re-apply the mask

    img_np = circular_mask_vectorise(img_np)

    inner_r = list(range(0, 650, 5))
    outer_r = list(range(10, 655, 5))

    while True:
        for r, r_ in zip(inner_r, outer_r):
            mk = make_mask(r, r_, RAW_IMG_SIZE)
            img_np[mk, :] -= img_np[mk, :].mean(axis=(0, 1))

        current_mean = np.abs(img_np.mean(axis=(0, 1)))
        if (current_mean.mean() < flux_lim):
            break

    return K.utils.image_to_tensor(img_np, keepdim=True)


class SohoComet(Dataset):
    """
    Class to load comet data from the train_data folder.
    Lots of proprocessing steps needs to be done.

    """

    def __init__(self, config, data_dir, set_type="train"):
        '''
        Defines the data access part for the training pipeline
        '''
        self.device = config["device"]
        self.folder = config['folder']
        self.resize_frac = RESIZE_FRAC
        self.folders = data_dir
        self.comet_file_seq_dict = create_comet_sequences(self.folders)
        self.comet_seq_path = []
        self.comet_tracks = []
        for comet, comet_seqs in self.comet_file_seq_dict.items():
            fits_files = np.asarray(
                sorted(glob.glob(self.folder+comet+'/*.fts')))
            comet_pos_data = pd.read_csv(self.folder+comet+'/'+comet+'.txt', skiprows=5, names=['date', 'time', 'filename', 'X', 'Y', 'mag'],
                                         header=None, delim_whitespace=True)
            for seq in comet_seqs['file_seqs']:
                self.comet_seq_path.append(fits_files[seq])
                # as comet track select X and Y column of the dataframe (column 3 and 4)
                self.comet_tracks.append(comet_pos_data.iloc[seq, 3:5].values)

    def __len__(self):
        return len(self.comet_seq_path)

    def __getitem__(self, idx):

        image_paths = self.comet_seq_path[idx]
        data_arr, mean_, median_ = img_seq_load(image_paths)
        img_tensor = K.utils.image_to_tensor(data_arr, keepdim=True)
        img_min_max = K.enhance.normalize_min_max(img_tensor, eps=1e-4)
        img_eq = K.enhance.equalize_clahe(
            img_min_max, clip_limit=100., grid_size=(3, 3))
        img_cube_sub = bck_sub_circular_normalize_batch_vectorise(
            img_eq-img_eq.std(axis=1), 0.01)
        img_reshape = K.geometry.rescale(
            img_cube_sub, (self.resize_frac, self.resize_frac), interpolation='nearest', antialias=True)
        heatmaps = vector_to_heatmaps(self.comet_tracks[idx]/RAW_IMG_SIZE)
        keypoints = torch.from_numpy(self.comet_tracks[idx]/RAW_IMG_SIZE)
        heatmaps = torch.from_numpy(np.float32(heatmaps))

        return {
            "image": img_reshape,
            "keypoints": keypoints,
            "heatmaps": heatmaps
        }
