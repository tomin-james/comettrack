import numpy as np
import os
import glob
from PIL import Image
from astropy.io import fits
import cv2
import matplotlib.pyplot as plt
import pandas as pd
import numpy.typing as npt
from typing import List, Dict, Tuple

RESIZE_FRAC = 0.250
N_KEYPOINTS = 3
N_IMG_CHANNELS = 3
RAW_IMG_SIZE = 1024
MODEL_IMG_SIZE = int(RAW_IMG_SIZE*RESIZE_FRAC)
MODEL_NEURONS = 16


def strided_app(a: npt.ArrayLike, L: int, S: int) -> npt.NDArray:
    # Window len = L, Stride len/stepsize = S
    '''
    Function to split the files into chunks of n size
    The idea is to pass in these chunks sequentially,
    with window_len and stride_len being user-defined
    quantities. 
    Taken from https://stackoverflow.com/a/40085052

    Input:
    a : input array should be 1D
    L : window_len
        length of individual chunk
    S : stride_len
        index to split the next chunk

    Output:
    Returns a 2D array with each row containing a sequential split 
    To be used for making sequential split of files to create input for the model
    '''
    # calculate how many times the array can be divided
    # for the given window_len and stride_len
    nrows: int = ((a.size-L)//S)+1

    # np.array is stored as contigous memory blocks
    # stride controls how many memory locations to skip
    n: int = a.strides[0]

    return np.lib.stride_tricks.as_strided(a, shape=(nrows, L), strides=(S*n, n))


def create_train_test_split(L: int, ratio: float = 0.8) -> npt.ArrayLike:
    '''
    Takes in as input the number of folders available
    as training comets and the required ratio of the split.
    Outputs two arrays containing the indexes for train and validatioin 
    splits of the folders to be used.
    '''

    perm_order = np.random.permutation(L)
    train_ = perm_order[:int(ratio*L)]
    val_ = perm_order[int(ratio*L):]
    return train_, val_


def create_comet_sequences(dir_paths: List) -> Dict:
    '''
    Creates possible file sequences for a given comet. |
    The possible file sequences are depended on the number 
    of files available.
    A sequence requires a minimum of 3 files.
    If the number of files available is more than 5, then, we
    look for more extended strides

    Input is a list containing a path to the folder containing comet files
    Output is a dicitonary with the key value corresponding to the name of 
    of the comet folder.

    '''

    comets = {}

    for comet_path in dir_paths:
        temp = {}
        # find the number of files with .fts extension in the folder
        no_fits_files = len(sorted(glob.glob(comet_path+'/*.fts')))
        # only consider comets which contains more than 3 points
        if no_fits_files > 3:
            # plane vanilla sequences ->[[0,1,2],[1,2,3]]
            seq_file_nos: npt.ArrayLike = strided_app(
                np.arange(0, no_fits_files), 3, 1)
            file_seqs = np.concatenate((seq_file_nos,
                                        # sequences with one number apart ->[[0,2,4]]
                                        strided_app(
                                            np.arange(0, no_fits_files, 2), 3, 1),
                                        # sequences with one number apart starting from 1 ->[[1,3,5]]
                                        strided_app(
                                            np.arange(1, no_fits_files, 2), 3, 1),
                                        # sequences with three number apart -> [[0,3,6]]
                                        strided_app(np.arange(0, no_fits_files, 3), 3, 1)))

            # only for comets which contains more than 5 points
            # this check is needed to account or the case where we are creating
            # sequences 5 points apart
            if no_fits_files > 5:
                file_seqs = np.concatenate((file_seqs,
                                            strided_app(
                                                np.arange(0, no_fits_files, 4), 3, 1),
                                            strided_app(np.arange(0, no_fits_files, 5), 3, 1)))
            temp['file_seqs'] = file_seqs
            temp['seq_len'] = len(file_seqs)
            comets[comet_path.split('/')[-1]] = temp

    return comets


def normalize_image_vectorise(img_arr: npt.NDArray, smax: float) -> npt.NDArray:
    '''
    A simple utility function to enable normalizing of an image
    between 0 and a user defined upper level.
    If possible always use K.minmax from Kornia
    '''
    smin = 0.0
    img_min, img_max = img_arr.min(axis=(0, 1)), img_arr.max(axis=(0, 1))
    img_arr = (((img_arr - img_min) * (smax - smin)) /
               (img_max - img_min)) + smin
    return img_arr


def bck_sub_normalize(flux: npt.ArrayLike, flux_lim: float) -> npt.ArrayLike:
    '''
    An iterative flux removal utility to remove RFI noises.
    The flux substraction is done on the frequency axis. 
    '''
    start_mean = flux.mean()
    while True:
        temp_Data_I = flux - np.mean(flux, axis=0)
        flux = temp_Data_I.clip(min=0)
        if (((start_mean - flux.mean())/start_mean) >= flux_lim) & (flux.mean() > flux_lim):
            start_mean = flux.mean()
        else:
            break
    flux = ((flux - flux.min())/(flux.max()-flux.min()))*255
    return flux


def circular_mask(arr, val=0):
    '''
    Function to create circular mask with a set value on
    top of an image.
    '''
    a, b = 512, 512
    n = 1024
    r = 200
    r_ = 650
    val = arr.mean()
    y, x = np.ogrid[-a:n-a, -b:n-b]
    mask = x*x + y*y >= r_*r_
    arr[mask] = val
    mask = x*x + y*y <= r*r
    arr[mask] = val

    return arr


def circular_mask_vectorise(arr, val=0):
    '''
    Function to create circular mask with a set value on
    top of an image.

    vectorised version of circular_mask func.
    '''
    a, b = 512, 512
    n = 1024
    r = 200
    r_ = 650
    val = arr.mean(axis=(0, 1))
    y, x = np.ogrid[-a:n-a, -b:n-b]
    mask = x*x + y*y >= r_*r_
    arr[mask, :] = val
    mask = x*x + y*y <= r*r
    arr[mask, :] = val

    return arr


def vector_to_heatmaps(keypoints: npt.ArrayLike) -> npt.NDArray:
    """
    Creates 2D heatmaps from keypoint locations for a single image
    Input: array of size N_KEYPOINTS x 2
    Output: array of size N_KEYPOINTS x MODEL_IMG_SIZE x MODEL_IMG_SIZE
    """
    heatmaps = np.zeros([N_KEYPOINTS, MODEL_IMG_SIZE, MODEL_IMG_SIZE])
    for k, (x, y) in enumerate(keypoints):
        x, y = int(x * MODEL_IMG_SIZE), int(y * MODEL_IMG_SIZE)
        if (0 <= x < MODEL_IMG_SIZE) and (0 <= y < MODEL_IMG_SIZE):
            heatmaps[k, int(y), int(x)] = 1

    heatmaps = blur_heatmaps(heatmaps)
    return heatmaps


def blur_heatmaps(heatmaps: npt.ArrayLike):
    """Blurs heatmaps using GaussinaBlur of defined size"""
    heatmaps_blurred = heatmaps.copy()
    for k in range(len(heatmaps)):
        if heatmaps_blurred[k].max() == 1:
            heatmaps_blurred[k] = cv2.GaussianBlur(heatmaps[k], (51, 51), 5)
            heatmaps_blurred[k] = heatmaps_blurred[k] / \
                heatmaps_blurred[k].max()
    return heatmaps_blurred


def heatmaps_to_coordinates(heatmaps: npt.NDArray) -> npt.ArrayLike:
    """
    Heatmaps is a numpy array
    Its size - (batch_size, n_keypoints, img_size, img_size)
    """
    batch_size = heatmaps.shape[0]
    sums = heatmaps.sum(axis=-1).sum(axis=-1)
    sums = np.expand_dims(sums, [2, 3])
    normalized = heatmaps / sums
    x_prob = normalized.sum(axis=2)
    y_prob = normalized.sum(axis=3)

    arr = np.tile(np.float32(np.arange(0, 1024)), [batch_size, 3, 1])
    x = (arr * x_prob).sum(axis=2)
    y = (arr * y_prob).sum(axis=2)
    keypoints = np.stack([x, y], axis=-1)
    return keypoints


def img_seq_load(img_paths: npt.ArrayLike) -> Tuple[npt.NDArray, npt.ArrayLike, npt.ArrayLike]:
    '''
    A function to load and standardize image data from the list of 
    paths. This function is used to load the data for every training sample.
    Returns the data cube with size (no:of images, 1024,1024)
    The mean of the data cube
    The median of the data cube

    The mean and the median is later used to do background substraction
    '''

    # number of paths to file
    # this is dependent on how many comet sequence we are trying to track per sample
    nf = len(img_paths)

    # Create 3D data cube to hold data, assuming all LASCO C2 data have
    # array sizes of 1024x1024 pixels.
    data_cube = np.empty((RAW_IMG_SIZE, RAW_IMG_SIZE, nf))

    # Create an empty list to hold date/time values, which are later used for output png filenames
    dates_times = []

    for i in range(nf):
        # read image and header from FITS file
        img, hdr = fits.getdata(img_paths[i], header=True)

        # Normalize by exposure time (a good practice for LASCO data)
        img = img.astype('float64') / hdr['EXPTIME']
        # Store array into datacube (3D array)
        data_cube[:, :, i] = img

    data_cube = circular_mask_vectorise(data_cube)
    data_cube = normalize_image_vectorise(data_cube, 255)
    mean, std = data_cube.mean(axis=(0, 1)), data_cube.std(axis=(0, 1))
    img_cube = (data_cube-mean)/std

    mean_img = img_cube.mean(axis=2)
    median_img = np.median(img_cube, axis=2)

    return img_cube, mean_img, median_img


def make_mask(r: int, r_: int, n: int) -> npt._BoolLike_co:
    '''
    A function to calculate the pixels falling in between two circles
    of radius r and r_, with r_>r. 

    '''

    a, b = n//2, n//2

    y, x = np.ogrid[-a:n-a, -b:n-b]
    mask = x*x + y*y >= r_*r_
    mask1 = x*x + y*y >= r*r

    return mask1 != mask
